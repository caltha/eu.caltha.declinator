<?php

function civicrm_api3_declinator_gender($params) {
  $start = microtime(TRUE);
  $femaleKey = CRM_Declinator_Settings::femaleId();
  $maleKey = CRM_Declinator_Settings::maleId();
  $map = [
    'K' => $femaleKey ,
    'M' => $maleKey,
  ];
  $query = "SELECT
              c.id, df.gender
            FROM
              civicrm_contact c
              JOIN declinator_first df ON df.nominative = c.first_name
                AND (df.gender <> (CASE
                                   WHEN c.gender_id = %1 THEN 'K'
                                   WHEN c.gender_id = %2 THEN 'M'
                                   ELSE NULL END) OR c.gender_id IS NULL)
            WHERE c.gender_id IN (%1, %2) OR c.gender_id IS NULL";
  $queryParams = [
    1 => [$femaleKey, 'Integer'],
    2 => [$maleKey, 'Integer'],
  ];
  $dao = CRM_Core_DAO::executeQuery($query, $queryParams);
  while ($dao->fetch()) {
    $queryUpdate = "UPDATE civicrm_contact SET gender_id = %2 WHERE id = %1";
    $paramsUpdate = [
      1 => [$dao->id, 'Integer'],
      2 => [$map[$dao->gender], 'String'],
    ];
    CRM_Core_DAO::executeQuery($queryUpdate, $paramsUpdate);
    CRM_Declinator_Hook::updateGender($dao->id);
  }
  $results = array(
    'count' => $dao->N,
    'time' => microtime(TRUE) - $start,
  );
  return civicrm_api3_create_success($results, $params);
}
