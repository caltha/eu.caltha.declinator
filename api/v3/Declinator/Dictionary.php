<?php

function _civicrm_api3_declinator_dictionary_spec(&$spec) {
  $spec['sqlfile'] = array(
    'name' => 'sqlfile',
    'title' => 'Ścieżka do pliku SQL ze słownikiem imion',
    'description' => 'Ścieżka do pliku SQL ze słownikiem imion',
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => 1,
    'api.default' => '',
  );
}

/**
 * Aktualizacja słownika imion.
 * @param $params
 *
 * @return array
 */
function civicrm_api3_declinator_dictionary(&$params) {
  $start = microtime(TRUE);
  $sqlQuery = file_get_contents($params['sqlfile'], TRUE);
  CRM_Declinator_Upgrader::executeCustomSql($sqlQuery);

  $extraReturnValues = array(
    'time' => microtime(TRUE) - $start,
  );
  $blank = NULL;
  return civicrm_api3_create_success(1, $params, NULL, NULL, $blank, $extraReturnValues);
}
