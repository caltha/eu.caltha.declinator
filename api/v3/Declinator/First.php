<?php

/**
 * @param $params
 *
 * @return array
 * @throws \Exception
 */
function civicrm_api3_declinator_first($params) {
  $start = microtime(TRUE);
  $hash = date('YmdHis') . str_pad(random_int(1, 999999), 6, "0", STR_PAD_LEFT);
  $query = "SELECT declinatorUpdate(%1) AS results;";
  $queryParams = [
    1 => [$hash, 'String'],
  ];
  $count = (int) CRM_Core_DAO::singleValueQuery($query, $queryParams);
  $query = "SELECT entity_id FROM civicrm_value_declinator_first WHERE hash = %1";
  $dao = CRM_Core_DAO::executeQuery($query, $queryParams);
  while ($dao->fetch()) {
    CRM_Declinator_Hook::updateFirst($dao->entity_id);
  }
  $results = array(
    'count' => $count,
    'time' => microtime(TRUE) - $start,
  );
  return civicrm_api3_create_success($results, $params);
}
