# Declinator

Rozszerzenie dostarcza następujące funkcjonalności:

1. automatyczne ustawianie odmiany imienia
2. automatyczne ustawienie płci na podstawie imienia
3. możliwość ręcznego ustawienia odmiany nazwiska

W tym celu rozszerzenie tworzy dwie grupy pól dodatkowych widocznych jako zakładki na stronie kontaktu:

* `Odmiana imienia` z 7 polami do odmiany imion
* `Odmiana nazwiska` z 7 polami do odmiany nazwisk

Podczas instalacji rozszerzenia tworzone są dzienne zaplanowane zadania, które są domyślnie nieaktywne:

* zadanie `Aktualizacja odmian imion`,
* zadanie `Aktualizacja płci`,
* zadanie `Aktualizacja słownika imion`.

## Jak to działa?

Rozszerzenie bazuje na słowniku imion, w którym mianownik jest porównywany z imieniem z kontaktu. Jeśli w słowniku znajduje się szukane imię wówczas może nastąpić aktualizacja odmian, czyli wszystkich przypadków. Aktualizacja następuje po spełnieniu poniższych warunków:

* pola z odmianami są puste lub
* nastąpiła zmiana imienia, tzn. imię kontaktu różni się od mianownika

Takie reguły pozwalają na ręczne ustawienie odmiany, która nie zostanie nadpisana przez zaplanowane zadanie cykliczne aktualizacji imion.

Dodatkowo dzięki zadaniu `Aktualizacja płci` możliwa jest aktualizacja płci kontaktu na podstawie imienia. Płeć określona jest w słowniku imion.

Ważnym elementem jest słownik imion, który po instalacji zawiera listę 50 najpopularniejszych imion. Słownik można wzbogacić poprzez wykonanie zapytania `SQL` bezpośrednio na bazie danych lub poprzez skorzystanie z zadania `Aktualizacja słownika imion`. W tym zadaniu podajemy ścieżkę na serwerze do pliku z zapytaniami, które będą wykonywane raz dziennie.

## Przykład użycia - pozdrowienie e-mailowe

Na stronie do zarządzania typami pozdrowień e-mailowych należy dodać nowy typ, w którym wykorzystamy pole `Wołacz (o!)` z grupy pól dodatkowych `Odmiana imienia`.

W pierwszej kolejności należy zidentyfikować ID tego pola dodatkowego. Możemy to zrobić przez podejrzenie linka do edycji pola. Załóżmy, że w naszym przypadku jest to 69. Wówczas tworzymy nowy typ pozdrowienia z następującą treścią:

`Dzień dobry {custom_69}!`

Teraz należy ustawić nowy typ pozdrowienia na stronie kontaktu.

## API

### declinator.first

* Aktualizacja odmian imion
* parametry: brak

### declinator.gender

* Aktualizacja płci
* parametry: brak

### declinator.dictionary

* Aktualizacja słownika imion
* parametry: `sqlfile` - ścieżka na serwerze do pliku z zapytaniami `SQL` aktualizującymi słownik.
