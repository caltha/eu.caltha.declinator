<?php

/**
 * Collection of upgrade steps.
 */
class CRM_Declinator_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * @throws \CiviCRM_API3_Exception
   */
  public function install() {
    $sqlQuery = file_get_contents(dirname(__FILE__) . '/../../sql/custom.sql', TRUE);
    self::executeCustomSql($sqlQuery);

    $params = [
      'sequential' => 1,
      'api_entity' => "Declinator",
      'api_action' => "first",
      'run_frequency' => "Daily",
      'name' => "Aktualizacja odmian imion",
      'is_active' => 0,
    ];
    civicrm_api3('Job', 'create', $params);

    $params = [
      'sequential' => 1,
      'api_entity' => "Declinator",
      'api_action' => "gender",
      'run_frequency' => "Daily",
      'name' => "Aktualizacja płci",
      'is_active' => 0,
    ];
    civicrm_api3('Job', 'create', $params);
  }

  /**
   * @throws \CiviCRM_API3_Exception
   */
  public function postInstall() {
    $this->upgrade_011_dictionary();
    $this->upgrade_020_hook();
    $this->upgrade_023_fix_declinator_update();

    return TRUE;
  }

  /**
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public function upgrade_011_dictionary() {
    $params = [
      'sequential' => 1,
      'api_entity' => "Declinator",
      'api_action' => "dictionary",
      'run_frequency' => "Daily",
      'name' => "Aktualizacja słownika imion",
      'is_active' => 0,
      'parameters' => 'sqlfile=/data.sql --popraw bezwzględną ścieżkę do pliku'
    ];
    civicrm_api3('Job', 'create', $params);
    return TRUE;
  }

  /**
   * @return bool
   */
  public function upgrade_020_hook() {
    $sqlQuery = file_get_contents(dirname(__FILE__) . '/../../sql/020-hook.sql', TRUE);
    self::executeCustomSql($sqlQuery);

    return TRUE;
  }

  /**
   * @return bool
   */
  public function upgrade_023_fix_declinator_update() {
    $sqlQuery = file_get_contents(__DIR__ . '/../../sql/023-fix-declinatorUpdate.sql', TRUE);
    self::executeCustomSql($sqlQuery);

    return TRUE;
  }

  public static function executeCustomSql($sqlQuery) {
    $string = CRM_Utils_File::stripComments($sqlQuery);
    $queries = preg_split('/@@@$/m', $string);
    foreach ($queries as $query) {
      $query = trim($query);
      if (!empty($query)) {
        CRM_Core_DAO::executeQuery($query);
      }
    }
  }

}
