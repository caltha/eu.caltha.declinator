<?php

class CRM_Declinator_Hook {

  static $null = NULL;

  /**
   * This hook is called after declinator first name update
   * @param int $contactId
   *
   * @return mixed
   */
  public static function updateFirst($contactId) {
    return CRM_Utils_Hook::singleton()->invoke(['contactId'], $contactId,
      self::$null, self::$null, self::$null, self::$null, self::$null,
      'civicrm_declinatorUpdateFirst');
  }

  /**
   * This hook is called after declinator gender update
   * @param int $contactId
   *
   * @return mixed
   */
  public static function updateGender($contactId) {
    return CRM_Utils_Hook::singleton()->invoke(['contactId'], $contactId,
      self::$null, self::$null, self::$null, self::$null, self::$null,
      'civicrm_declinatorUpdateGender');
  }

}
