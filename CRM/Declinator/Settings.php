<?php

class CRM_Declinator_Settings {

  const CACHE_PREFIX = 'eu.caltha.declinator.';

  /**
   * Get gender id for female.
   *
   * @return int|mixed
   */
  public static function femaleId() {
    $key = self::CACHE_PREFIX . __FUNCTION__;
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = 1;
      Civi::cache()->set($key, $id);
      return $id;
    }
    return $cache;
  }

  /**
   * Get gender id for male.
   *
   * @return int|mixed
   */
  public static function maleId() {
    $key = self::CACHE_PREFIX . __FUNCTION__;
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = 2;
      Civi::cache()->set($key, $id);
      return $id;
    }
    return $cache;
  }

}
