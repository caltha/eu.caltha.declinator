DROP TABLE IF EXISTS declinator_first;
CREATE TABLE declinator_first (
  nominative varchar(128) NOT NULL PRIMARY KEY COMMENT 'Mianownik (kto?, co?)',
  genitive varchar(128) DEFAULT NULL COMMENT 'Dopełniacz (kogo?, czego?)',
  dative varchar(128) DEFAULT NULL COMMENT 'Celownik (o kim?, o czym?)',
  accusative varchar(128) DEFAULT NULL COMMENT 'Biernik (kogo?, co?)',
  instrumental varchar(128) DEFAULT NULL COMMENT 'Narzędnik (z kim?, z czym?)',
  locative varchar(128) DEFAULT NULL COMMENT 'Miejscownik (o kim?, o czym?)',
  vocative varchar(128) DEFAULT NULL COMMENT 'Wołacz (o!)',
  gender CHAR(1) NOT NULL COMMENT 'K - kobieta, M - mężczyzna',
  KEY declinator_first_gender_inx (gender)
) DEFAULT CHARSET=utf8mb4
  COLLATE=utf8mb4_unicode_ci
  COMMENT 'Słownik odmian imion';

DROP TABLE IF EXISTS declinator_last;
CREATE TABLE declinator_last (
  nominative varchar(128) NOT NULL PRIMARY KEY COMMENT 'Mianownik (kto?, co?)',
  genitive varchar(128) DEFAULT NULL COMMENT 'Dopełniacz (kogo?, czego?)',
  dative varchar(128) DEFAULT NULL COMMENT 'Celownik (o kim?, o czym?)',
  accusative varchar(128) DEFAULT NULL COMMENT 'Biernik (kogo?, co?)',
  instrumental varchar(128) DEFAULT NULL COMMENT 'Narzędnik (z kim?, z czym?)',
  locative varchar(128) DEFAULT NULL COMMENT 'Miejscownik (o kim?, o czym?)',
  vocative varchar(128) DEFAULT NULL COMMENT 'Wołacz (o!)',
  gender CHAR(1) NOT NULL COMMENT 'K - kobieta, M - mężczyzna',
  KEY declinator_last_gender_inx (gender)
) DEFAULT CHARSET=utf8mb4
  COLLATE=utf8mb4_unicode_ci
  COMMENT 'Słownik odmian nazwisk';

DROP TABLE IF EXISTS declinator_contact;
CREATE TABLE declinator_contact (
  id int(10) unsigned PRIMARY KEY COMMENT 'Unique Contact ID',
  first_name varchar(64) DEFAULT NULL COMMENT 'First Name.',
  last_name varchar(64) DEFAULT NULL COMMENT 'Last Name.',
  gender_id int(10) UNSIGNED DEFAULT NULL COMMENT 'FK to gender ID'
) DEFAULT CHARSET=utf8mb4
  COLLATE=utf8mb4_unicode_ci
  COMMENT 'Kopia danych potrzebnych do aktualizacji, wymagana ze względu na wyzwalacze i zakleszczenia';

INSERT IGNORE INTO declinator_first (nominative, genitive, dative, accusative, instrumental, locative, vocative, gender)
VALUES
  ('Tomasz', 'Tomasza', 'Tomaszowi', 'Tomasza', 'Tomaszem', 'Tomaszu', 'Tomaszu', 'M'),
  ('Katarzyna', 'Katarzyny', 'Katarzynie', 'Katarzynę', 'Katarzyną', 'Katarzynie', 'Katarzyno', 'K'),
  ('Anna', 'Anny', 'Annie', 'Annę', 'Anną', 'Annie', 'Anno', 'K'),
  ('Agnieszka', 'Agnieszki', 'Agnieszce', 'Agnieszkę', 'Agnieszką', 'Agnieszce', 'Agnieszko', 'K'),
  ('Joanna', 'Joanny', 'Joannie', 'Joannę', 'Joanną', 'Joannie', 'Joanno', 'K'),
  ('Magdalena', 'Magdaleny', 'Magdalenie', 'Magdalenę', 'Magdaleną', 'Magdalenie', 'Magdaleno', 'K'),
  ('Marta', 'Marty', 'Marcie', 'Martę', 'Martą', 'Marcie', 'Marto', 'K'),
  ('Aleksandra', 'Aleksandry', 'Aleksandrze', 'Aleksandrę', 'Aleksandrą', 'Aleksandrze', 'Aleksandro', 'K'),
  ('Małgorzata', 'Małgorzaty', 'Małgorzacie', 'Małgorzatę', 'Małgorzatą', 'Małgorzacie', 'Małgorzato', 'K'),
  ('Karolina', 'Karoliny', 'Karolinie', 'Karolinę', 'Karoliną', 'Karolinie', 'Karolino', 'K'),
  ('Monika', 'Moniki', 'Monice', 'Monikę', 'Moniką', 'Monice', 'Moniko', 'K'),
  ('Michał', 'Michała', 'Michałowi', 'Michała', 'Michałem', 'Michale', 'Michale', 'M'),
  ('Ewa', 'Ewy', 'Ewie', 'Ewę', 'Ewą', 'Ewie', 'Ewo', 'K'),
  ('Piotr', 'Piotra', 'Piotrowi', 'Piotra', 'Piotrem', 'Piotrze', 'Piotrze', 'M'),
  ('Agata', 'Agaty', 'Agacie', 'Agatę', 'Agatą', 'Agacie', 'Agato', 'K'),
  ('Marcin', 'Marcina', 'Marcinowi', 'Marcina', 'Marcinem', 'Marcinie', 'Marcinie', 'M'),
  ('Paulina', 'Pauliny', 'Paulinie', 'Paulinę', 'Pauliną', 'Paulinie', 'Paulino', 'K'),
  ('Paweł', 'Pawła', 'Pawłowi', 'Pawła', 'Pawłem', 'Pawle', 'Pawle', 'M'),
  ('Justyna', 'Justyny', 'Justynie', 'Justynę', 'Justyną', 'Justynie', 'Justyno', 'K'),
  ('Natalia', 'Natalii', 'Natalii', 'Natalię', 'Natalią', 'Natalii', 'Natalio', 'K'),
  ('Łukasz', 'Łukasza', 'Łukaszowi', 'Łukasza', 'Łukaszem', 'Łukaszu', 'Łukaszu', 'M'),
  ('Maria', 'Marii', 'Marii', 'Marię', 'Marią', 'Marii', 'Mario', 'K'),
  ('Maciej', 'Macieja', 'Maciejowi', 'Macieja', 'Maciejem', 'Macieju', 'Macieju', 'M'),
  ('Krzysztof', 'Krzysztofa', 'Krzysztofowi', 'Krzysztofa', 'Krzysztofem', 'Krzysztofie', 'Krzysztofie', 'M'),
  ('Barbara', 'Barbary', 'Barbarze', 'Barbarę', 'Barbarą', 'Barbarze', 'Barbaro', 'K'),
  ('Jakub', 'Jakuba', 'Jakubowi', 'Jakuba', 'Jakubem', 'Jakubie', 'Jakubie', 'M'),
  ('Mateusz', 'Mateusza', 'Mateuszowi', 'Mateusza', 'Mateuszem', 'Mateuszu', 'Mateuszu', 'M'),
  ('Dorota', 'Doroty', 'Dorocie', 'Dorotę', 'Dorotą', 'Dorocie', 'Doroto', 'K'),
  ('Dominika', 'Dominiki', 'Dominice', 'Dominikę', 'Dominiką', 'Dominice', 'Dominiko', 'K'),
  ('Beata', 'Beaty', 'Beacie', 'Beatę', 'Beatą', 'Beacie', 'Beato', 'K'),
  ('Sylwia', 'Sylwii', 'Sylwii', 'Sylwię', 'Sylwią', 'Sylwii', 'Sylwio', 'K'),
  ('Magda', 'Magdy', 'Magdzie', 'Magdę', 'Magdą', 'Magdzie', 'Magdo', 'K'),
  ('Alicja', 'Alicji', 'Alicji', 'Alicję', 'Alicją', 'Alicji', 'Alicjo', 'K'),
  ('Elżbieta', 'Elżbiety', 'Elżbiecie', 'Elżbietę', 'Elżbietą', 'Elżbiecie', 'Elżbieto', 'K'),
  ('Izabela', 'Izabeli', 'Izabeli', 'Izabelę', 'Izabelą', 'Izabeli', 'Izabelo', 'K'),
  ('Iwona', 'Iwony', 'Iwonie', 'Iwonę', 'Iwoną', 'Iwonie', 'Iwono', 'K'),
  ('Adam', 'Adama', 'Adamowi', 'Adama', 'Adamem', 'Adamie', 'Adamie', 'M'),
  ('Kamila', 'Kamili', 'Kamili', 'Kamilę', 'Kamilą', 'Kamili', 'Kamilo', 'K'),
  ('Patrycja', 'Patrycji', 'Patrycji', 'Patrycję', 'Patrycją', 'Patrycji', 'Patrycjo', 'K'),
  ('Ewelina', 'Eweliny', 'Ewelinie', 'Ewelinę', 'Eweliną', 'Ewelinie', 'Ewelino', 'K'),
  ('Olga', 'Olgi', 'Oldze', 'Olgę', 'Olgą', 'Oldze', 'Olgo', 'K'),
  ('Grzegorz', 'Grzegorza', 'Grzegorzowi', 'Grzegorza', 'Grzegorzem', 'Grzegorzu', 'Grzegorzu', 'M'),
  ('Bartosz', 'Bartosza', 'Bartoszowi', 'Bartosza', 'Bartoszem', 'Bartoszu', 'Bartoszu', 'M'),
  ('Kamil', 'Kamila', 'Kamilowi', 'Kamila', 'Kamilem', 'Kamilu', 'Kamilu', 'M'),
  ('Aneta', 'Anety', 'Anecie', 'Anetę', 'Anetą', 'Anecie', 'Aneto', 'K'),
  ('Martyna', 'Martyny', 'Martynie', 'Martynę', 'Martyną', 'Martynie', 'Martyno', 'K'),
  ('Marek', 'Marka', 'Markowi', 'Marka', 'Markiem', 'Marku', 'Marku', 'M'),
  ('Wojciech', 'Wojciecha', 'Wojciechowi', 'Wojciecha', 'Wojciechem', 'Wojciechu', 'Wojciechu', 'M'),
  ('Andrzej', 'Andrzeja', 'Andrzejowi', 'Andrzeja', 'Andrzejem', 'Andrzeju', 'Andrzeju', 'M'),
  ('Rafał', 'Rafała', 'Rafałowi', 'Rafała', 'Rafałem', 'Rafale', 'Rafale!', 'M');
