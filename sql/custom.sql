DROP FUNCTION IF EXISTS declinatorUpdate@@@
CREATE FUNCTION declinatorUpdate() RETURNS INT
  BEGIN
    DECLARE fr, rcUpdateEmpty, rcUpdateNominative INT;

    DROP TEMPORARY TABLE IF EXISTS declinator_temp;
    CREATE TEMPORARY TABLE declinator_temp (
      id int(10) unsigned PRIMARY KEY COMMENT 'Unique Contact ID',
      nominative varchar(128) NOT NULL COMMENT 'Mianownik (kto?, co?)',
      genitive varchar(128) DEFAULT NULL COMMENT 'Dopełniacz (kogo?, czego?)',
      dative varchar(128) DEFAULT NULL COMMENT 'Celownik (o kim?, o czym?)',
      accusative varchar(128) DEFAULT NULL COMMENT 'Biernik (kogo?, co?)',
      instrumental varchar(128) DEFAULT NULL COMMENT 'Narzędnik (z kim?, z czym?)',
      locative varchar(128) DEFAULT NULL COMMENT 'Miejscownik (o kim?, o czym?)',
      vocative varchar(128) DEFAULT NULL COMMENT 'Wołacz (o!)'
    ) COMMENT 'Temporary table used for updating custom fields';

    -- update when nominative exists
    UPDATE civicrm_value_declinator_first cv
      JOIN declinator_first df ON df.nominative = cv.nominative
    SET
      cv.genitive = IF(cv.genitive IS NULL OR cv.genitive = '', df.genitive, cv.genitive),
      cv.dative = IF(cv.dative IS NULL OR cv.dative = '', df.dative, cv.dative),
      cv.accusative = IF(cv.accusative IS NULL OR cv.accusative = '', df.accusative, cv.accusative),
      cv.instrumental = IF(cv.instrumental IS NULL OR cv.instrumental = '', df.instrumental, cv.instrumental),
      cv.locative = IF(cv.locative IS NULL OR cv.locative = '', df.locative, cv.locative),
      cv.vocative = IF(cv.vocative IS NULL OR cv.vocative = '', df.vocative, cv.vocative)
    WHERE (cv.genitive IS NULL OR cv.genitive = '') OR
        (cv.dative IS NULL OR cv.dative = '') OR
        (cv.accusative IS NULL OR cv.accusative = '') OR
        (cv.instrumental IS NULL OR cv.instrumental = '') OR
        (cv.locative IS NULL OR cv.locative = '') OR
        (cv.vocative IS NULL OR cv.vocative = '');

    SELECT ROW_COUNT() INTO rcUpdateNominative;

    -- update empty row
    INSERT INTO declinator_temp
      SELECT
        c.id, df.nominative, IF(cv.genitive IS NULL OR cv.genitive = '', df.genitive, cv.genitive),
        IF(cv.dative IS NULL OR cv.dative = '', df.dative, cv.dative),
        IF(cv.accusative IS NULL OR cv.accusative = '', df.accusative, cv.accusative),
        IF(cv.instrumental IS NULL OR cv.instrumental = '', df.instrumental, cv.instrumental),
        IF(cv.locative IS NULL OR cv.locative = '', df.locative, cv.locative),
        IF(cv.vocative IS NULL OR cv.vocative = '', df.vocative, cv.vocative)
      FROM civicrm_value_declinator_first cv
        JOIN declinator_contact c ON c.id = cv.entity_id AND (cv.nominative IS NULL OR cv.nominative = '')
        JOIN declinator_first df ON df.nominative = c.first_name;
    UPDATE civicrm_value_declinator_first cv
      JOIN declinator_temp t ON t.id = entity_id
    SET
      cv.nominative = t.nominative,
      cv.genitive = t.genitive,
      cv.dative = t.dative,
      cv.accusative = t.accusative,
      cv.instrumental = t.instrumental,
      cv.locative = t.locative,
      cv.vocative = t.vocative;
    SELECT ROW_COUNT() INTO rcUpdateEmpty;

    -- insert new
    DELETE FROM declinator_temp;
    INSERT INTO declinator_temp
      SELECT c.id, df.nominative, df.genitive, df.dative, df.accusative, df.instrumental, df.locative, df.vocative
      FROM declinator_contact c
        LEFT JOIN civicrm_value_declinator_first cv ON cv.entity_id = c.id
        JOIN declinator_first df ON df.nominative = c.first_name
      WHERE cv.id IS NULL AND c.id > 0;

    INSERT INTO civicrm_value_declinator_first (entity_id, nominative, genitive, dative, accusative, instrumental, locative, vocative)
      SELECT * FROM declinator_temp;
    SELECT FOUND_ROWS() INTO fr;

    DROP TEMPORARY TABLE IF EXISTS declinator_temp;
    RETURN fr + rcUpdateEmpty + rcUpdateNominative;
  END@@@

INSERT IGNORE INTO declinator_contact (id, first_name, last_name, gender_id)
  SELECT id, first_name, last_name, gender_id
  FROM civicrm_contact
  WHERE contact_type = 'Individual';
