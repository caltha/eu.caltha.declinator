<?php

require_once 'declinator.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function declinator_civicrm_config(&$config) {
  _declinator_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function declinator_civicrm_install() {
  _declinator_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function declinator_civicrm_enable() {
  _declinator_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_triggerInfo().
 *
 * @link https://docs.civicrm.org/dev/en/master/hooks/hook_civicrm_triggerInfo/
 */
function declinator_civicrm_triggerInfo(&$info, $tableName) {
  $sourceTable = 'civicrm_contact';
  $sqlInsertUpdate = "
    INSERT INTO declinator_contact (id, first_name, last_name, gender_id)
    VALUES (NEW.id, NEW.first_name, NEW.last_name, NEW.gender_id)
    ON DUPLICATE KEY UPDATE first_name = NEW.first_name, last_name = NEW.last_name, gender_id = NEW.gender_id;
";
  $sqlDelete = "
    DELETE FROM declinator_contact WHERE id = OLD.id;
";

  $info[] = array(
    'table' => $sourceTable,
    'when' => 'BEFORE',
    'event' => 'INSERT',
    'sql' => $sqlInsertUpdate,
  );
  $info[] = array(
    'table' => $sourceTable,
    'when' => 'BEFORE',
    'event' => 'UPDATE',
    'sql' => $sqlInsertUpdate,
  );
  $info[] = array(
    'table' => $sourceTable,
    'when' => 'BEFORE',
    'event' => 'DELETE',
    'sql' => $sqlDelete,
  );
}

// /**
//  * Implements hook_civicrm_entityTypes().
//  *
//  * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
//  */
// function declinator_civicrm_entityTypes(&$entityTypes) {
//   _declinator_civix_civicrm_entityTypes($entityTypes);
// }
